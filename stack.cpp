#include <iostream>
#include "stack.h"
#include "linkedList.h"
using namespace std;

void initStack(stack* s)
{
	int myElement = 0;
	cout << "Please enter element: ";
	cin >> myElement;
	s->head = NULL;
	s->head = createNodeToIn(s->head, myElement);
}

linkedList* createNodeToIn(linkedList* s, int myElement)
{
	linkedList* newNode = new linkedList;

	newNode->element = myElement;
	newNode->next = s;
	s = newNode;

	return newNode;
}

void push(stack* s, unsigned int element)
{
	s->head = createNodeToIn(s->head, int(element));
}

int pop(stack* s)
{
	int empty = 0;
	s->head = s->head->next;
	while (!(s->head))
	{
		empty = IS_EMPTY;
	}
	return empty;
}

void cleanStack(stack* s)
{
	linkedList* node = NULL;
	while(s->head)
	{
		node = s->head;
		s->head = s->head->next;
		delete(node);
	}
}

void printStack(linkedList* head)
{
	linkedList* curr = head;
	cout << "\nThe stack from the last to enter to the first to enter:\n";
	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		cout << curr->element;
		cout << "\n";
		curr = curr->next;
	}
	cout << "\n";
}