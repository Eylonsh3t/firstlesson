#pragma once
#include <iostream>
#include <bits.h>
#include "utils.h"
using namespace std;

#define ARRAY_SIZE 6
#define DYNAMIC_ARRAY_SIZE 10

int main()
{
	int arr[] = { 1, 2, 3, 4, 5, 6 };
	int* dArr = NULL;
	unsigned int size = ARRAY_SIZE;
	cout << "The array:\n";
	printArray(arr, size);
	reverse(arr, int(size) - 1);
	cout << "\nThe reversed array:\n";
	printArray(arr, size);
	dArr = reverse10();
	cout << "\nThe reversed array:\n";
	printArray(dArr, DYNAMIC_ARRAY_SIZE);

	delete[] dArr;
	return 0;
}