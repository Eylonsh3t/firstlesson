#ifndef QUEUE_H
#define QUEUE_H

#define QUEUE_EMPTY -1

/* a queue contains positive integer values. */

typedef struct queue
{
	int* queueArray;
	unsigned int size;
	int numbersInQueue;
	unsigned int newValue;
	int isEmpty;
} queue;

void initQueue(queue* q, unsigned int size);
void cleanQueue(queue* q);

void enqueue(queue* q, unsigned int newValue);
int dequeue(queue* q); // return element in top of queue, or -1 if empty
void queueSize(queue* q);
void newValue(queue* q);
void printMenu();
void printQueue(queue* q);
int userChoice();
queue* createQueue();

#endif /* QUEUE_H */