#pragma once
#include <iostream>
#include "stack.h"
#include "linkedList.h"
using namespace std;

int main()
{
	stack* s = new stack;
	initStack(s);
	push(s, 2);
	printStack(s->head);
	pop(s);
	printStack(s->head);
	cleanStack(s);
	delete(s);
	return 0;
}
