#pragma once
#include <iostream>
#include "linkedList.h"
using namespace std;

linkedList* createNodeToInsert()
{
	linkedList* newNode = new linkedList;
	
	newNode->next = NULL;
	do
	{
		cout << "\nInsert positive number you want to add: ";
		cin >> newNode->element;
	} while (newNode->element < 0);

	return newNode;
}

void insertToStack(linkedList** head, linkedList* newNode)
{
	linkedList* curr = *head;
	linkedList* next = NULL;
	linkedList* tempNode;

	if (!*head) // empty list!
	{
		*head = newNode;
	}
	else
	{
		next = curr;
		tempNode = newNode;
		*head = newNode;
		curr = *head;
		curr->next = next;
	}
}

void deleteNodeFromStack(linkedList** head)
{
	*head = (*head)->next;
}

void printStack(linkedList* head)
{
	linkedList* curr = head;
	cout << "\nThe stack from the last to enter to the first to enter:\n";
	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		cout << curr->element;
		cout << "\n";
		curr = curr->next;
	}
	cout << "\n";
}