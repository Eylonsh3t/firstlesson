#pragma once

typedef struct linkedList
{
	unsigned int element;
	struct linkedList* next;
} linkedList;

void insertToStack(linkedList** head, linkedList* newNode);
void deleteNodeFromStack(linkedList** head);
linkedList* createNodeToInsert();
void printStack(linkedList* head);