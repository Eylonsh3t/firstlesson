#ifndef UTILS_H
#define UTILS_H


void reverse(int* nums, unsigned int size);
int* reverse10();
void printArray(int* nums, unsigned int size);

#endif // UTILS_H