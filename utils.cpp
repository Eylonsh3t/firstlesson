#include <iostream>
#include <bits.h>
#include "utils.h"
using namespace std;

#define ARRAY_SIZE 6
#define DYNAMIC_ARRAY_SIZE 10

void reverse(int* nums, unsigned int size)
{
	//Code from: https://www.geeksforgeeks.org/write-a-program-to-reverse-an-array-or-string/
	int start = 0;
	while (start < int(size))
	{
		int temp = nums[start];
		nums[start] = nums[size];
		nums[size] = temp;
		start++;
		size--;
	}
}

void printArray(int* nums, unsigned int size)
{
	for (int i = 0; i < int(size); i++)
	{
		cout << nums[i] << "	";
	}
}

int* reverse10()
{
	int i = 0;
	int start = 0;
	int size = DYNAMIC_ARRAY_SIZE - 1;
	int* nums = new int[DYNAMIC_ARRAY_SIZE];
	cout << "\n";
	for (i = 0; i < DYNAMIC_ARRAY_SIZE; i++)
	{
		cout << "\nEnter number: ";
		cin >> nums[i];
	}

	while (start < size)
	{
		int temp = nums[start];
		nums[start] = nums[size];
		nums[size] = temp;
		start++;
		size--;
	}
	return nums;
}