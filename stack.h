#pragma once
#include "linkedList.h"

#define IS_EMPTY -1

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	linkedList* head;
	struct stack* next;
} stack;

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty
void initStack(stack* s);
void cleanStack(stack* s);
void printStack(linkedList* head);
linkedList* createNodeToIn(linkedList* s, int myElement);