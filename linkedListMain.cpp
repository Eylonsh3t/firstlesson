#pragma once
#include <iostream>
#include "linkedList.h"
using namespace std;

int main()
{
	linkedList* head = NULL;
	linkedList* newNode = NULL;

	newNode = createNodeToInsert();
	insertToStack(&head, newNode);
	printStack(head);
	newNode = createNodeToInsert();
	insertToStack(&head, newNode);
	printStack(head);
	deleteNodeFromStack(&head);
	printStack(head);

	delete(newNode);
	return 0;
}