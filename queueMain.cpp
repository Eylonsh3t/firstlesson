#pragma once
#include <iostream>
#include "Queue.h"
using namespace std;

int main()
{
	queue* q = createQueue();
	int choice = 0;
	do {
		printMenu();
		choice = userChoice();
		switch (choice)
		{
		case 1:
			cout << "You chose option 1\n";
			newValue(q);
			enqueue(q, q->newValue);
			break;
		case 2:
			cout << "You chose option 2\n";
			dequeue(q);
			break;
		case 3:
			cout << "You chose option 3\n";
			printQueue(q);
			break;
		default:
			cout << "Goodbye :)";
			break;
		}
	} while (choice == 1 || choice == 2 || choice == 3);
	return 0;
}